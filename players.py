import random


class MatchHistory(object):

    def __init__(self):
        self.matches = []

    def add_match(self, **kwargs):
        self.matches.append(kwargs)

    def draw(self):
        for match in self.matches:
            print(match)

    def was_author(self):
        was_author = 0
        for match in self.matches:
            if match['author']:
                was_author += 1
        return was_author

    def __str__(self):
        return '%s' % self.matches


class Player(object):

    def __init__(self, name, rank=random.randint(1000, 3000)):
        self.name = name

        self.rating = rank
        self.wins = random.randint(1, 999)
        self.loses = random.randint(1, 999)

        self.priority = {'player': random.randint(0, 1), 'author': False}
        self.in_queue = False
        self.in_game = False
        self.in_lobby = False
        self.time_in_queue = 0
        self.pos_in_queue = 0
        self.current_game = None
        self.current_game_result = {'Score': 0, 'Round': [], 'Result': []}

        self.match_history = MatchHistory()

        if self.priority['player'] == 0:
            self.priority['author'] = 1

    def __str__(self):
        return '[%s]: r: %d, w: %d, l: %d, p: %s, time: %d, mh: %s' % (
            self.name,
            self.rating,
            self.wins,
            self.loses,
            self.priority,
            self.time_in_queue,
            self.match_history)

    def reset(self):
        self.in_queue = False
        self.in_game = False
        self.time_in_queue = 0
        self.pos_in_queue = 0
        self.current_game = None
        self.current_game_result = {'Score': 0, 'Round': [], 'Result': []}
