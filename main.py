import time
import random
import threading

from matchmaking import Matchmaking, Player
from dev import dev_logger

queue = Matchmaking()


def game():
    player = Player('Kubix')
    while True:
        print('Welcome %s' % player.name)
        _input = int(input('1. Join q, 2. Leave q, 3. Results'))
        if _input == 1:
            if not player.in_queue:
                queue.join(player)
            else:
                if player.in_game:
                    print('You are in game')

        elif _input == 2:
            if player.in_queue:
                queue.leave(player)
            else:
                print('You are not in queue')
        elif _input == 3:
            print(player)


def player_func():
    while True:
        players = []
        for x in range(5000):
            players.append(Player('John%d' % random.randint(1, 99999), random.randint(1000, 3000)))

        for player in players:
            queue.join(player)

        break


player_threader = threading.Thread(target=player_func, name='Player Thread')
queue_threader = threading.Thread(target=queue.start, name='Queue Thread')
timer_threader = threading.Thread(target=queue.update_timer,
                                  name='Timer Thread')
# game_threader = threading.Thread(target=game, name='Game Thread')
timer_threader.daemon = True
player_threader.daemon = True
queue_threader.daemon = True
# game_threader.daemon = True

player_threader.start()
queue_threader.start()
timer_threader.start()
# game_threader.start()

player_threader.join()
queue_threader.join()
timer_threader.join()
# game_threader.join()
