import time
import random
import statistics
import threading

from players import Player
from dev import dev_logger


class Matchmaking(object):

    def __init__(self):
        self.queue = []
        self.players_to_start = 5

        self.time = time
        self.games = 0
        self.pl = 0

    def join(self, player: Player):
        self.pl += 1
        self.queue.append(player)
        player.in_queue = True
        player.time_in_queue = 0
        player.pos_in_queue = self.pl

    def start(self):
        lobbies = GameLobbies()
        self.time.sleep(5)

        while True:
            for player in self.queue:
                if not player.in_lobby:
                    search_lobby = lobbies.find_lobby(player)

                    if not search_lobby:
                        lobby = GameLobby(player.rating)
                        lobbies.add(lobby)
                        lobby.join(player)
                    else:
                        search_lobby.join(player)

                    self.leave(player)

    @staticmethod
    def get_average_rating(game_queue):
        avg_stat = []
        for player in game_queue:
            avg_stat.append(player.rating)

        avg = statistics.mean(avg_stat)
        return avg

    @staticmethod
    def get_average_time(data):
        avg_time = []
        for x in data:
            avg_time.append(x.time_in_queue)

        avg = statistics.mean(avg_time)
        return avg

    def update_timer(self):
        while True:
            for player in self.queue:
                player.time_in_queue += 1
            self.time.sleep(1)

    def leave(self, player: Player):
        self.queue.remove(player)
        player.reset()
        return


class GameLobby(object):

    def __init__(self, avg_rank):
        self.lobby_rank = avg_rank
        self.players = []
        self.max_players = 5
        self.state = 0  # 0 = waiting for players, 1 = game started
        self.name = 'Lobby #%d' % random.randint(100000, 999999)

        dev_logger.info('Lobby %s created' % self.name)

    def join(self, player: Player):
        dev_logger.info('%s joined to lobby %s' % (player, self.name))
        self.players.append(player)
        self.resum_ranking()
        player.in_queue = False
        player.in_lobby = True

        if self.__size__() >= self.max_players:
            self.state = 1
            GameObject(self)

    def __size__(self):
        return len(self.players)

    def rank(self):
        return self.lobby_rank

    def leave(self, player: Player):
        self.players.remove(player)

    def resum_ranking(self):
        avg_stat = []
        for player in self.players:
            avg_stat.append(player.rating)

        avg = statistics.mean(avg_stat)
        self.lobby_rank = avg

    def __str__(self):
        return self.name


class GameObject(object):
    """every game object must have thread(?)"""

    def __init__(self, lobby: GameLobby):
        self.lobby = lobby
        self.author = None
        self.round = 1
        self.game_subject = "NULL"
        self.preset = [
            'Make a photo',
            'Make a video',
            'Drink water',
            'Eat dinner',
            'Jump',
            'Sit',
            'Prone',
        ]

        self.game = threading.Thread(target=self.start, name=self.lobby)
        self.game.daemon = True
        self.game.start()
        self.game.join()

    def start(self):
        authors_list = []
        for player in self.lobby.players:
            player.time_in_queue = 0
            player.in_queue = False
            player.in_game = True

            # If player want to be author in priority
            # add him on authors list
            if player.priority['author'] == 1:
                authors_list.append(player)

        # if no players wants to be author
        # add all players to list based on their match history
        # if player wasn't be author, he will be in priority
        if not authors_list:
            previous = None

            for x in self.lobby.players:
                # If player wasn't be author
                # instantly add him on authors list
                if not x.match_history.was_author():
                    authors_list.append(x)
                else:
                    # if current player was author less times that previous
                    # add him on list and delete previous player
                    if x.match_history.was_author() < \
                            previous.match_history.was_author():
                        authors_list.append(x)
                        authors_list.remove(previous)

                previous = x

        self.author = random.choice(authors_list)

    def __str__(self):
        return 'Game: %s' % self.lobby


class GameLobbies(object):

    def __init__(self):
        self.lobbies = []
        dev_logger.info('Lobbies initiated')

    def add(self, lobby: GameLobby):
        self.lobbies.append(lobby)

    def delete(self, lobby: GameLobby):
        self.lobbies.remove(lobby)

    """searching lobby for a player"""

    def find_lobby(self, player: Player):
        for lobby in self.lobbies:
            if lobby.state <= 0:
                if (player.rating + player.time_in_queue) >= lobby.rank() >= (
                        player.rating - player.time_in_queue):
                    if lobby.__size__() < lobby.max_players:
                        return lobby
            elif lobby.state == 1:
                self.delete(lobby)
        return None
