import logging

logging.basicConfig(filename='game.log',
                    filemode='a',
                    format='[%(asctime)s] %(levelname)s: %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)
__version__ = '0.01a_0001'

dev_logger = logging.getLogger("dev")

with open('game.log', 'w') as f:
    f.close()
